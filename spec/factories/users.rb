FactoryBot.define do
  factory :user, class: Api::V1::Ar::User do
    factory :test_user do
      login { 'Test User Login' }
    end
  end
end
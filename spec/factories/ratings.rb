FactoryBot.define do
  factory :rating, class: Api::V1::Ar::Rating do
    trait :rating_1 do
      rate { 1 }
    end

    trait :rating_2 do
      rate { 2 }
    end

    trait :rating_3 do
      rate { 3 }
    end

    trait :rating_4 do
      rate { 4 }
    end

    trait :rating_5 do
      rate { 5 }
    end
  end
end
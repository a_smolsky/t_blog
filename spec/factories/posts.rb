FactoryBot.define do
  factory :post, class: Api::V1::Ar::Post do
    factory :user_post do
      title { 'Test title' }
      description { 'Test Description' }
      ip { '192.168.1.100' }
    end
  end
end
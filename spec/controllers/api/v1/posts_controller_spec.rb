require 'spec_helper'

describe Api::V1::PostsController do
  let(:ar_user_class) { Api::V1::Ar::User }
  let(:ar_post_class) { Api::V1::Ar::Post }
  let(:ar_rating_class) { Api::V1::Ar::Rating }
  let(:post_creator_class) { Api::V1::Posts::PostCreator }
  let(:post_handler_class) { Api::V1::Posts::PostHandler }
  let(:post_rating_class) { Api::V1::Posts::PostRatingProcessor }

  describe 'create new post' do
    let(:params) do
      {
        login: 'Login',
        title: 'Title',
        description: 'Description',
        ip: 'Ip'
      }
    end
    let(:user_new_post_params) do
      {
        login: 'Login',
        title: 'Title_2',
        description: 'Description_2',
        ip: 'Ip_2'
      }
    end

    it 'creates post' do
      expect_no_user(params)
      expect_no_posts

      create_action(params)

      user = ar_user_class.find_by(login: params[:login])
      result = parse_response(response.body)

      expect(response.status).to eq(status_success)
      expect(result[:id]).to be > 0
      expect(result[:user_id]).to be > 0
      expect(result[:title]).to eq(params[:title])
      expect(result[:description]).to eq(params[:description])
      expect(result[:ip]).to eq(params[:ip])
      expect(user).not_to be_nil
      expect(user.login).to eq(params[:login])
    end

    it 'finds already existed user and creates a new post' do
      expect_no_user(params)
      expect_no_posts

      create_action(params)
      post_1 = find_post(response.body)

      create_action(user_new_post_params)
      post_2 = find_post(response.body)

      expect(ar_post_class.count).to eq(2)
      expect(post_1.user_id).to eq(post_2.user_id)

      expect(post_1.id).not_to eq(post_2.id)
      expect(post_1.title).not_to eq(post_2.title)
      expect(post_1.description).not_to eq(post_2.description)
      expect(post_1.ip).not_to eq(post_2.ip)
    end

    it 'raises posts title required' do
      params[:title] = nil

      create_action(params)
      result = parse_response(response.body)

      expect(response.status).to eq(status_validation_error)
      expect(result).to eq("#{post_creator_class} Error: Post Title required.")
    end

    it 'raises posts title too long' do
      params[:title] = 256.times.inject('') { |title| title + ('a'..'z').to_a.sample }

      create_action(params)
      result = parse_response(response.body)

      expect(response.status).to eq(status_validation_error)
      expect(result).to eq("#{post_creator_class} Error: Post title too long.")
    end

    it 'raises posts title required' do
      params[:description] = nil

      create_action(params)
      result = parse_response(response.body)

      expect(response.status).to eq(status_validation_error)
      expect(result).to eq("#{post_creator_class} Error: Post Description required.")
    end
  end

  describe 'post authors' do
    it 'ignores ips from where only one author wrote' do
      expect_no_posts

      user = create(:test_user)
      5.times { create(:user_post, user_id: user.id) }

      index_action
      result = parse_response(response.body)

      expect(result).to be_empty
    end

    it 'finds ips from where more then one autror wrote' do
      expect_no_posts

      user_1 = create(:test_user)
      user_2 = create(:test_user, login: 'New_login')

      expect(ar_user_class.count).to eq(2)

      post_1 = create(:user_post, user_id: user_1.id)
      post_2 = create(:user_post, user_id: user_2.id)

      expect(ar_post_class.count).to eq(2)

      index_action
      result = parse_response(response.body)

      expected_result = {
        post_1.ip => [
          user_1.login,
          user_2.login
        ]
      }

      expect(result).not_to be_empty
      expect(post_1.ip).to eq(post_2.ip)
      expect(expected_result).to eq(result)
    end
  end

  describe 'top posts' do
    let(:rate_1) { create(:rating, :rating_1) }
    let(:rate_2) { create(:rating, :rating_2) }
    let(:rate_3) { create(:rating, :rating_3) }
    let(:user) { create(:test_user) }
    let(:params) { {amount: 2} }

    it 'returns N top posts' do
      expect_no_posts

      post_1 = create(:user_post,post_params(user, 1))
      post_2 = create(:user_post, post_params(user, 2))
      post_3 = create(:user_post, post_params(user, 3))

      rate_post(post_3, rate_3)
      rate_post(post_2, rate_2)
      rate_post(post_1, rate_1)

      show_action(params)
      result = parse_response(response.body)

      expect(result).to be_a_kind_of(Hash)
      expect(result.keys.first).to eq(post_3.id.to_s)
      expect(result[post_2.id.to_s][:title]).to eq(post_2.title)
      expect(result[post_2.id.to_s][:description]).to eq(post_2.description)
      expect(result[post_2.id.to_s][:rate].to_i).to eq(rate_2.rate)
      expect(result[post_3.id.to_s][:title]).to eq(post_3.title)
      expect(result[post_3.id.to_s][:description]).to eq(post_3.description)
      expect(result[post_3.id.to_s][:rate].to_i).to eq(rate_3.rate)
    end

    it 'raises incorrect posts amount' do
      expect_no_posts
      params[:amount] = 'String'

      show_action(params)
      result = parse_response(response.body)

      expect(response.status).to eq(status_validation_error)
      expect(result).to eq("#{post_handler_class} Error: Incorrect posts amount.")
    end
  end

  describe 'post rating' do
    let(:user) { create(:test_user) }
    let(:post) { create(:user_post, user_id: user.id) }
    let(:rate_1) { create(:rating, :rating_1) }
    let(:rate_2) { create(:rating, :rating_2) }
    let(:rates) { [rate_1.rate, rate_2.rate] }

    it 'rates the post' do
      params = {
        id: post.id,
        rate: rate_1.rate
      }

      update_action(params)

      params = {
        id: post.id,
        rate: rate_2.rate
      }

      update_action(params)
      result = parse_response(response.body)
      expected_result = (rates.sum.to_f)/rates.size

      expect(result).to eq(expected_result)
    end

    it 'raises incorrect post_id' do
      params = {id: 'String'}

      update_action(params)
      result = parse_response(response.body)

      expect(response.status).to eq(status_validation_error)
      expect(result).to eq("#{post_rating_class} Error: Incorrect post id.")
    end

    it 'raises incorrect rate format' do
      params = {
        id: post.id,
        rate: 'String'
      }

      update_action(params)
      result = parse_response(response.body)

      expect(response.status).to eq(status_validation_error)
      expect(result).to eq("#{post_rating_class} Error: Incorrect rate value.")
    end

    it 'raises incorrect rate value' do
      params = {
          id: post.id,
          rate: 6
      }

      update_action(params)
      result = parse_response(response.body)

      expect(response.status).to eq(status_validation_error)
      expect(result).to eq("#{post_rating_class} Error: Incorrect rate. Allowed ratings: 1, 2, 3, 4, 5.")
    end
  end

  private

  def index_action
    get :index
  end

  def show_action(params)
    get :show, params
  end

  def create_action(params)
    post :create, params
  end

  def update_action(params)
    put :update, params
  end

  def expect_no_user(params)
    user = ar_user_class.find_by(login: params[:login])
    expect(user).to be_nil
  end

  def expect_no_posts
    expect(ar_post_class.count).to eq(0)
  end

  def find_post(response)
    post = JSON.parse(response).with_indifferent_access
    ar_post_class.find post[:id]
  end

  def parse_response(json_response)
    response = JSON.parse(json_response)
    response.is_a?(Hash) ? response.with_indifferent_access : response
  end

  def post_params(user, post_number)
    {
      user_id: user.id,
      title: "Title_#{post_number}",
      description: "Description_#{post_number}"
    }
  end

  def rate_post(post, rate)
    post.ratings << rate
    post.save
  end

  def status_success
    200
  end

  def status_validation_error
    422
  end
end
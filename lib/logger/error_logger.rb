module ErrorLogger

  def log_error(object)
    yield

  rescue => e
    message = "#{object.class} Error: #{e.message}"
    status = e.is_a?(Api::V1::Posts::ApiValidatorError) ? 422 : 500

    object.error[:message] = message.to_json
    object.error[:status] = status
  end

  def on_success
    yield if error.empty?
  end

  def on_failure
    yield unless error.empty?
  end

end
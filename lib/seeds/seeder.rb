require 'faker'

module Seeds
  class Seeder
    attr_reader :users_count, :posts_count, :ar_user_class, :ar_post_class, :ar_rating_class, :fake_ips, :posts_per_user, :max_batch

    MAX_BATCH_SIZE = 3_000

    def initialize(users_count: 10, user_posts_count: 2_000,
                   ar_user_class: Api::V1::Ar::User,
                   ar_post_class: Api::V1::Ar::Post,
                   ar_rating_class:  Api::V1::Ar::Rating)

      @users_count = users_count
      @posts_count = user_posts_count
      @ar_user_class = ar_user_class
      @ar_post_class = ar_post_class
      @ar_rating_class = ar_rating_class
      @fake_ips = prepare_fake_ips(50)
      @posts_per_user = user_posts_count/users_count
      @max_batch = [MAX_BATCH_SIZE, @posts_per_user].min
    end

    def create_test_data
      puts 'Start...'

      ActiveRecord::Base.transaction do
        user_ids = create_users
        create_posts(user_ids)
        rate_posts
      end

      puts 'Finish...'
    end

    private

    def create_users
      fake_users = (1..users_count).each_with_object([]) do |_, res|
        res << {login: fake_user_name}
      end

      result = ar_user_class.create(fake_users)
      result.map(&:id)
    end

    def create_posts(ids)
      processed = 0

      ids.each do |user_id|
        user_posts_batch = []
        batches = posts_batches(processed)

        batches.each do |batch|
          batch.times do
            post = {
              user_id: user_id,
              title: Faker::Lorem.sentence(rand(1..5), true),
              description: Faker::Lorem.paragraph(5),
              ip: fake_user_ip
            }
            user_posts_batch << post
          end

          processed += batch

          ar_post_class.create(user_posts_batch)

          notify_result(processed, :posts)
        end
      end
    end

    def rate_posts
      puts 'Started posts evaluation...'

      ratings = ar_rating_class.all.to_a
      ratings_size = ratings.size

      posts = ar_post_class.limit(100)
      posts.each do |post|
        ratings_size.size.times do
          post.ratings << ratings[rand(ratings_size)]
          post.save!
        end
      end

      puts 'Finished posts evaluation...'
    end

    def fake_ratings
      (1..5).to_a
    end

    def fake_user_name
      rand(5) > 0 ? Faker::Internet.user_name : ''
    end

    def fake_user_ip
      fake_ips[rand(fake_ips.size)]
    end

    def prepare_fake_ips(count)
      return unless count > 0
      no_ip = ''
      ips = [no_ip]

      (1..(count-1)).each_with_object(ips) { |_, res| res << Faker::Internet.ip_v4_cidr }
    end

    def notify_result(count, model)
      message =
        case model
        when :posts
          "Posts creating: #{count} of #{posts_count} #{model} were created..."
        when :rates
          "Post Ratings creating: Rates for #{count} of #{posts_count} were created... "
        end

      puts message
    end

    def posts_batches(processed)
      batches = []

      if posts_per_user <= max_batch
        batch_size = post_batch(processed, posts_per_user)
        batches << batch_size
      else
        full_batches_count = posts_per_user/MAX_BATCH_SIZE

        full_batches_count.times do
          batches << MAX_BATCH_SIZE
        end

        batches << posts_per_user%MAX_BATCH_SIZE if posts_per_user%MAX_BATCH_SIZE > 0
      end

      batches
    end

    def post_batch(processed, batch_step)
      processed + batch_step > posts_count ? (posts_count - processed) : batch_step
    end

  end
end
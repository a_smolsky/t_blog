require 'rake/testtask'

namespace :unit_no_rails do
  Rake::TestTask.new(:test) do |t|
    t.description = 'Runs the unit tests without Rails.'
    t.warning = false
    t.libs << 'test'
    t.test_files = FileList['test/unit/**/*_test.rb']
  end
end
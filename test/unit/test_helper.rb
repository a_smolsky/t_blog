require 'minitest/autorun'

app_root = File.expand_path('../../../', __FILE__)

Dir[File.expand_path("#{app_root}/{app/models,app/helpers,lib}")].each do |path|
  $LOAD_PATH.unshift(path)
end

MiniTest.after_run do
  if ENV['NO_RAILS'] && defined?(Rails) && Rails.try(:application, :initialized?)
    raise 'Rails was initialized by a unit test! Failing!'
  end
end
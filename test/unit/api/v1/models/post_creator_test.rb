require 'unit/test_helper'
require 'api/v1/posts/post_creator'

describe Api::V1::Posts::PostCreator do
  let(:successful_result) { 'Success' }
  let(:user_login_mock) { 'User Login' }
  let(:user_id) { :user_id }
  let(:params) do
    {
      login: user_login_mock,
      title: 'Title',
      description: 'Description',
      ip: 'Ip'
    }
  end
  let(:post_params) do
    {
      user_id: user_id,
      title: 'Title',
      description: 'Description',
      ip: 'Ip'
    }
  end
  let(:ar_class_mock) do
    mock = MiniTest::Mock.new
    mock.expect(:transaction, mock) { |&block| block.call }
  end
  let(:ar_post_class_mock) do
    mock = MiniTest::Mock.new
    mock.expect(:create, successful_result, [post_params])
  end
  let(:user_creator_class_mock) do
    mock = MiniTest::Mock.new
    mock.expect(:new, mock, [login: user_login_mock])
    mock.expect(:find_or_create_user, user_mock)
  end
  let(:validator_class_mock) do
    mock = MiniTest::Mock.new
    mock.expect(:new, mock, [params])
    mock.expect(:create_post_params, true)
  end
  let(:user_mock) do
    mock = MiniTest::Mock.new
    mock.expect(:id, user_id)
  end

  subject do
    Api::V1::Posts::PostCreator.new(
      params,
      ar_class: ar_class_mock,
      ar_post_class: ar_post_class_mock,
      validator_class: validator_class_mock,
      user_creator_class: user_creator_class_mock
    )
  end

  it 'creates a new post' do
    subject.stub(:log_error, nil) do
      subject.new_post

      ar_class_mock.verify
      ar_post_class_mock.verify
      user_creator_class_mock.verify
      validator_class_mock.verify
      user_mock.verify
      assert_equal(successful_result, subject.response[:output])
    end
  end
end
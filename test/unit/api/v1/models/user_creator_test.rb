require 'unit/test_helper'
require 'api/v1/posts/user_creator'

describe Api::V1::Posts::UserCreator do
  let(:success_result) { 'Success' }
  let(:user_login) { 'User Login' }
  let(:user_format) { context_user_format }
  let(:user_params_mock) do
    mock = MiniTest::Mock.new
    mock.expect(:user_format, user_format)

    def mock.is_a?(format)
      format == self.user_format ? true : false
    end

    mock.expect(:[], user_login, [:login])
  end
  let(:ar_user_class_mock) do
    mock = MiniTest::Mock.new
    mock.expect(:find_or_create_by, success_result, [login: user_login])
  end

  subject { Api::V1::Posts::UserCreator.new(user_params_mock, ar_user_class: ar_user_class_mock) }

  describe 'expected scenario' do
    let(:context_user_format) { Hash }

    it 'creates a new user' do
      result = subject.find_or_create_user

      user_params_mock.verify
      ar_user_class_mock.verify
      assert_equal(success_result, result)
    end
  end

  describe 'invalid user params' do
    let(:context_user_format) { Array }

    it 'raises user format error' do
      error = assert_raises(RuntimeError) { subject.find_or_create_user }
      assert_equal('Invalid user format' ,error.message)
    end
  end
end
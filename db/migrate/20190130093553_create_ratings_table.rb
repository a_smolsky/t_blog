class CreateRatingsTable < ActiveRecord::Migration
  TABLE_NAME = :ratings.freeze

  def up
    drop_table(TABLE_NAME) if table_exists?(TABLE_NAME)

    create_table(TABLE_NAME) do |t|
      t.integer :rate
    end
  end

  def down
    drop_table(TABLE_NAME) if table_exists?(TABLE_NAME)
  end
end

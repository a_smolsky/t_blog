class CreatePostsRatingsTable < ActiveRecord::Migration
  TABLE_NAME = :posts_ratings.freeze

  def up
    drop_table(TABLE_NAME) if table_exists?(TABLE_NAME)

    create_table(TABLE_NAME) do |t|
      t.belongs_to :rating, index: true
      t.belongs_to :post, index: true
    end
  end

  def down
    drop_table(TABLE_NAME) if table_exists?(TABLE_NAME)
  end

end

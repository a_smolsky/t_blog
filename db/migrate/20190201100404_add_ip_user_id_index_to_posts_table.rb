class AddIpUserIdIndexToPostsTable < ActiveRecord::Migration
  def up
    add_index :posts, [:ip, :user_id] unless index_exists?(:posts, [:ip, :user_id])
  end

  def down
    remove_index :posts, [:ip, :user_id] if index_exists?(:posts, [:ip, :user_id])
  end
end
class InsertDataIntoRatingsTable < ActiveRecord::Migration
  AR_MODEL = Api::V1::Ar::Rating
  AVAILABLE_RATINGS = (1..5)

  def up
    AR_MODEL.destroy_all

    AR_MODEL.create(AVAILABLE_RATINGS.map { |val| {rate: val} })
  end

  def down
    AR_MODEL.destroy_all
  end

end

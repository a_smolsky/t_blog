class CreatePostsTable < ActiveRecord::Migration
  TABLE_NAME = :posts.freeze

  def up
    drop_table(TABLE_NAME) if table_exists?(TABLE_NAME)

    create_table(TABLE_NAME) do |t|
      t.column :user_id, :bigint, unsigned: true, index: true
      t.string :title, limit: 255, null: false
      t.text :description, null: false
      t.string 'ip', limit: 255
    end
  end

  def down
    drop_table(TABLE_NAME) if table_exists?(TABLE_NAME)
  end
end

class CreateUsersTable < ActiveRecord::Migration
  TABLE_NAME = :users.freeze

  def up
    drop_table(TABLE_NAME) if table_exists?(TABLE_NAME)

    create_table(TABLE_NAME) do |t|
      t.string :login, limit: 255
    end
  end

  def down
    drop_table(TABLE_NAME) if table_exists?(TABLE_NAME)
  end
end

require "seeds/seeder"

module Seeds
  seeder = Seeder.new(users_count: 10, user_posts_count: 2_000)

  seeder.create_test_data
end
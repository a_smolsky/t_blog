<h1>Posts API</h1>
Test Project
Posts API provides opportunities to create a new post, to get number of existed most rated posts, about post authors their ips and logins.
<hr>
<h2>CREATE A NEW POST</h2>

Returns json data about newly created post.

* **URL**

  /posts
* **Method:**
  `POST`
*  **URL Params**
  None
* **Data Params**

  login=[String] Max length 255.<br />
  ip=[String] Max length 255.
  
  <b>Required:</b><br />
  title=[String] Max length 255. Post Title.<br />
  description=[Text] Post Message.
  
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ id : 12, title : "Title", description: "Description", ip: "100.100.100.100" }`
 
* **Error Response:**

  * **Code:** 500 INTERNAL SERVER ERROR <br />
  * **Code:** 422 VALIDATION ERROR <br />
  
* **SAMPLE CALL:**

  ```javascript
    $.ajax({
      url: "/posts",
      type : "POST",
      data: { login: "Login", title : "Title", description: "Description", ip: "100.100.100.100" }
      done : function(response) {
        console.log(response);
      }
    });
  ```
<hr>
<h2>SHOW TOP POSTS</h2>

Returns json data about requested number of top rated posts.

* **URL**

  /posts/:amount
* **Method:**
  `GET`
*  **URL Params**

   amount=[Integer] Number of required top posts.
* **Data Params**
  None
  
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ title : "Title1", description: "Description1", title : "Title2", description: "Description2" }`
 
* **Error Response:**

  * **Code:** 500 INTERNAL SERVER ERROR <br />
  * **Code:** 422 VALIDATION ERROR <br />
  
* **SAMPLE CALL:**

  ```javascript
    $.ajax({
      url: "/posts/10",
      type : "GET",
      done : function(response) {
        console.log(response);
      }
    });
  ```
<hr>
<h2>SHOW AUTHORS WHO POSTED FROM THE SAME IP</h2>

Returns json data about ips and authors.

* **URL**

  /posts
* **Method:**
  `GET`
*  **URL Params**
  None
* **Data Params**
  None
  
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ "ip1" : ["Login1", "Login2"], "ip2": ["Login3", "Login4"] }`
 
* **Error Response:**

  * **Code:** 500 INTERNAL SERVER ERROR <br />
  
* **SAMPLE CALL:**

  ```javascript
    $.ajax({
      url: "/posts",
      type : "GET",
      done : function(response) {
        console.log(response);
      }
    });
  ```
<hr>
<h2>RATE POST</h2>

Returns json data with a new rating of rated post.

* **URL**

  /posts/:id
* **Method:**
  `PUT`
*  **URL Params**

   id: Integer. Post ID.
* **Data Params**

  rate=[Integer] Allowed values: 1, 2, 3, 4 or 5
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ "1.11" }`
 
* **Error Response:**

  * **Code:** 500 INTERNAL SERVER ERROR <br />
  * **Code:** 422 VALIDATION ERROR <br />
  
* **SAMPLE CALL:**

  ```javascript
    $.ajax({
      url: "/posts/2",
      type : "PUT",
      data: { rate: 5 },
      done : function(response) {
        console.log(response);
      }
    });
  ```
Rails.application.routes.draw do

  scope module: :api do
    scope module: :v1 do
      resources :posts, only: [:index, :create, :update]
      resources :posts, only: [:show], param: :amount
    end
  end

end

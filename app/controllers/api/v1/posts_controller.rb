module Api
  module V1
    class PostsController < ApplicationController

      def index
        @presenter = Api::V1::Post.new({}).show_post_authors

        render_response(@presenter)
      end

      def create
        @presenter = Api::V1::Post.new(params).create_post

        render_response(@presenter)
      end

      def update
        @presenter = Api::V1::Post.new(params).rate_posts

        render_response(@presenter)
      end

      def show
        @presenter = Api::V1::Post.new(params).show_top_posts

        render_response(@presenter)
      end

      private

      def render_response(processor)
        processor.on_success do
          render json: processor.response[:output], status: 200
        end

        processor.on_failure do
          render json: processor.error[:message], status: processor.error[:status]
        end
      end

    end
  end
end
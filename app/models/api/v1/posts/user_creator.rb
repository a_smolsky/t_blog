module Api
  module V1
    module Posts
      class UserCreator
        attr_reader :user_params, :ar_user_class

        def initialize(user, ar_user_class: Api::V1::Ar::User)
          @user_params = user
          @ar_user_class = ar_user_class
        end

        def find_or_create_user
          raise 'Invalid user format' unless user_params.is_a?(Hash)

          ar_user_class.find_or_create_by(login: user_params[:login])
        end

      end
    end
  end
end
module Api
  module V1
    module Posts
      class PostHandler
        include ErrorLogger

        attr_reader :params, :ar_post_class, :validator, :error, :response

        def initialize(params = {},
                       ar_post_class: Api::V1::Ar::Post,
                       validator_class: Api::V1::Posts::ParamsValidator)

          @params = params
          @ar_post_class = ar_post_class
          @validator = validator_class
          @error = {}
          @response = {}
        end

        def authors_by_ips
          log_error(self) do
            response[:output] = ips_authors
          end

          self
        end

        def top_posts
          log_error(self) do
            validator.new(params).show_post_params
            top_posts = ActiveRecord::Base.connection.execute(top_posts_query(params[:amount]))

            response[:output] = top_posts_mapping(top_posts)
          end

          self
        end

        private

        def ips_authors
          result = ActiveRecord::Base.connection.execute(posts_by_ips_query)

          result.each_with_object({}) do |row, result|
            result[row['ip']] ||= []
            result[row['ip']] << row['login']
          end
        end

        def posts_by_ips_query
<<SQL
SELECT u.login, p1.ip
FROM users u
  JOIN (SELECT ip, user_id FROM posts GROUP BY ip, user_id) AS p1 ON u.id = p1.user_id
  JOIN 
    (SELECT p2.ip, COUNT(p2.user_id) FROM (SELECT ip, user_id FROM posts GROUP BY ip, user_id) AS p2
     GROUP BY p2.ip HAVING COUNT(p2.user_id) > 1) AS ips
ON p1.ip = ips.ip;
SQL
        end

        def top_posts_query(limit)
<<SQL
SELECT
  pr.post_id,
  p.title,
  p.description,  
  ROUND(AVG(r.rate)::numeric,2) as rate
FROM posts_ratings pr
  JOIN posts p ON p.id = pr.post_id
  JOIN ratings r ON pr.rating_id = r.id
GROUP BY pr.post_id, p.title, p.description
ORDER BY rate DESC
LIMIT #{limit}
SQL
        end

        def top_posts_mapping(posts)
          posts.each_with_object({}) do |post, output_hash|
            output_hash[post['post_id']]= {
              title: post['title'],
              description: post['description'],
              rate: post['rate']
            }
          end
        end

      end
    end
  end
end
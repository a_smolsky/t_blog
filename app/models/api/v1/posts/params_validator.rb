module Api
  module V1
    module Posts
      class ParamsValidator
        attr_reader :params

        TITLE_MAX_SIZE = 255
        ALLOWED_RATINGS = [1,2,3,4,5]

        def initialize(params)
          @params = params
        end

        def create_post_params
          raise(ApiValidatorError, 'Post Title required.') unless params[:title].present?
          raise(ApiValidatorError, 'Post title too long.') if long_title
          raise(ApiValidatorError, 'Post Description required.') unless params[:description].present?
        end

        def show_post_params
          raise(ApiValidatorError, 'Incorrect posts amount.') unless is_integer?(params[:amount])
        end

        def post_rating_params
          raise(ApiValidatorError, 'Incorrect post id.') unless is_integer?(params[:id])
          raise(ApiValidatorError, 'Incorrect rate value.') unless is_integer?(params[:rate].to_s)
          raise(ApiValidatorError, 'Incorrect rate. Allowed ratings: 1, 2, 3, 4, 5.') unless ALLOWED_RATINGS.include?(params[:rate].to_i)
        end

        private

        def is_integer?(param)
          param.match(/^\d+$/)
        end

        def long_title
          params[:title].size > TITLE_MAX_SIZE
        end

      end

      class ApiValidatorError < StandardError; end;
    end
  end
end
require 'logger/error_logger'

module Api
  module V1
    module Posts
      class PostCreator
        include ErrorLogger

        attr_reader :params, :ar_class, :validator, :ar_post_class,:user_creator_class, :error, :response

        def initialize(params,
                       ar_class: ActiveRecord::Base,
                       ar_post_class: Api::V1::Ar::Post,
                       validator_class: Api::V1::Posts::ParamsValidator,
                       user_creator_class: Api::V1::Posts::UserCreator)

          @params = params
          @validator = validator_class
          @ar_class = ar_class
          @ar_post_class = ar_post_class
          @user_creator_class = user_creator_class
          @error = {}
          @response = {}
        end

        def new_post
          log_error(self) do
            ar_class.transaction do
              validate_params
              user = user_creator_class.new(login: params[:login]).find_or_create_user

              response[:output] = create_post(post_params(user))
            end
          end

          self
        end

        private

        def create_post(post_params)
          ar_post_class.create(post_params)
        end

        def validate_params
          validator.new(params).create_post_params
        end

        def post_params(user)
          {
            user_id: user.id,
            title: params[:title],
            description: params[:description],
            ip: params[:ip]
          }
        end
      end
    end
  end
end
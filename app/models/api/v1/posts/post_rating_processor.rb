module Api
  module V1
    module Posts
      class PostRatingProcessor
        include ErrorLogger

        attr_reader :params, :ar_post_class, :ar_rating_class, :validator, :error, :response

        def initialize(params,
                       ar_rating_class: Api::V1::Ar::Rating,
                       ar_post_class: Api::V1::Ar::Post,
                       validator_class: Api::V1::Posts::ParamsValidator)

          @params = params
          @ar_rating_class = ar_rating_class
          @ar_post_class = ar_post_class
          @validator = validator_class
          @error = {}
          @response = {}
        end

        def rate_post
          log_error(self) do
            ActiveRecord::Base.transaction do
              validate_params

              post.ratings << rating
              post.save!

              ratings = post.ratings.map(&:rate)
              avg_rate = (ratings.sum.to_f/ratings.size.to_f).round(2)

              response[:output] = avg_rate
            end
          end

          self
        end

        private

        def validate_params
          validator.new(params).post_rating_params
        end

        def rating
          rate = ar_rating_class.find_by(rate: params[:rate])

          raise 'Invalid rating value. Allowed ratings: 1, 2, 3, 4, 5. Please try again.' unless rate&.id

          rate
        end

        def post
          post = ar_post_class.find_by(id: params[:id])

          raise 'Invalid post id. Please enter valid data.' unless post&.id

          post
        end

      end
    end
  end
end
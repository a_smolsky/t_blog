module Api
  module V1
    class Post
      attr_reader :params, :posts_creator, :post_handler, :rating_processor

      def initialize(post_params,
                     posts_creator_class: Api::V1::Posts::PostCreator,
                     show_authors_class: Api::V1::Posts::PostHandler,
                     post_rating_class:  Api::V1::Posts::PostRatingProcessor)

        @params = post_params
        @posts_creator = posts_creator_class
        @post_handler = show_authors_class
        @rating_processor = post_rating_class
      end

      def show_post_authors
        post_handler.new.authors_by_ips
      end

      def show_top_posts
        post_handler.new(params).top_posts
      end

      def create_post
        posts_creator.new(params).new_post
      end

      def rate_posts
        rating_processor.new(params).rate_post
      end

    end
  end
end
module Api
  module V1
    module Ar
      class ApplicationRecord < ActiveRecord::Base
        self.abstract_class = true
      end
    end
  end
end
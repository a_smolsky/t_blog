module Api
  module V1
    module Ar
      class Post < ApplicationRecord
        has_and_belongs_to_many :ratings
        belongs_to :user
      end
    end
  end
end
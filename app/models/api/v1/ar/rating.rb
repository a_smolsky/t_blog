module Api
  module V1
    module Ar
      class Rating < ApplicationRecord
        has_and_belongs_to_many :posts
      end
    end
  end
end
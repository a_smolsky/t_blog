module Api
  module V1
    module Ar
      class User < ApplicationRecord
        has_many :posts, dependent: :destroy
      end
    end
  end
end